﻿using DataComparer.Core.Interfaces.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.Core.Implementations.Infrastructure
{
    /// <summary>
    /// Represents single item which gives the information about dependency
    /// </summary>
    /// <typeparam name="TInterface">Interface which needs to be satisfied</typeparam>
    /// <typeparam name="TImplementation">Class which needs to be returned for the interface</typeparam>
    public class DependencyItem<TInterface, TImplementation> : IDependencyItem where TImplementation : TInterface
    {
        /// <summary>
        /// Interface which needs to be satisfied
        /// </summary>
        public Type Interface
        {
            get;
        }

        /// <summary>
        /// Class which needs to be returned for the interface
        /// </summary>
        public Type Implementation
        {
            get;
        }

        public DependencyItem()
        {
            Interface = typeof(TInterface);
            Implementation = typeof(TImplementation);
        }
    }
}
