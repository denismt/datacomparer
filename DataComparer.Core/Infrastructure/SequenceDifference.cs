﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.Core.Infrastructure
{
    /// <summary>
    /// Represents single item desribing difference in sequences
    /// </summary>
    public class SequenceDifference: IEquatable<SequenceDifference>
    {
        /// <summary>
        /// Start index of the difference
        /// </summary>
        public long StartIndex { get; set; }
        /// <summary>
        /// Length of the difference
        /// </summary>
        public long Length { get; set; }

        /// <summary>
        /// Checks if two differences are equal
        /// </summary>
        /// <param name="difference">SequenceDifference object to compare to</param>
        /// <returns>True if equals, false otherwise</returns>
        public bool Equals(SequenceDifference difference)
        {
            if (StartIndex != difference.StartIndex || Length != difference.Length)
                return false;
            return true;
        }
    }
}
