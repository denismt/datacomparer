﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DataComparer.Core.BusinessObjects
{
    /// <summary>
    /// Represents business object which contains all required data about comparison
    /// </summary>
    public class Comparison
    {
        [Key]
        public long Id { get; set; }
        /// <summary>
        /// String code of the object, which also may be colled "ID" in the UI and enpoints
        /// </summary>
        [Required]
        public string Code { get; set; }
        /// <summary>
        /// Left values that needs to be compared
        /// </summary>
        public byte[] LeftValue { get; set; }
        /// <summary>
        /// Right value that needs to be compared
        /// </summary>
        public byte[] RightValue { get; set; }
        /// <summary>
        /// Flag which whows if one of the values or both were changed since last results calculation
        /// </summary>
        [Required]
        [DefaultValue(true)]
        public bool IsChanged { get; set; }
        /// <summary>
        /// Navigation property for the related ComparisonResults
        /// </summary>
        protected virtual ICollection<ComparisonResult> ComparisonResults {get; set;}
    }
}
