﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.Core.BusinessObjects
{
    public class ComparisonResult
    {
        [Key]
        public long Id { get; set; }
        /// <summary>
        /// Start index of the different sequence
        /// </summary>
        [Required]
        public long Start { get; set; }
        /// <summary>
        /// Length of the different sequence
        /// </summary>
        [Required]
        public long Length { get; set; }
        /// <summary>
        /// Comarison object for that this result was calculated
        /// </summary>
        [Required]
        public long ComparisonId { get; set; }
        /// <summary>
        /// Navigation property for the related Comparison object
        /// </summary>
        protected virtual Comparison Comparison { get; set; }
    }
}
