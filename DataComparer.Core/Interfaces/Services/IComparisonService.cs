﻿using DataComparer.Core.BusinessObjects;
using DataComparer.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.Core.Interfaces.Services
{
    /// <summary>
    /// Represents signature for the classes which implements logic for Comparizon business object
    /// </summary>
    public interface IComparisonService
    {
        /// <summary>
        /// Gets specific comarison by its id
        /// </summary>
        /// <param name="id">Id of the required Comparison boject</param>
        /// <returns></returns>
        Comparison GetById(long id);
        /// <summary>
        /// Gets specific comarison by its string code (may be called id in UI and endpoints)
        /// </summary>
        /// <param name="code">Code of the required Comparison boject</param>
        /// <returns></returns>
        Comparison GetByCode(string code);
        /// <summary>
        /// Updates specific comparison objects with the specified left and right values
        /// </summary>
        /// <param name="code">String code (also may be called id in UI and enpoints) which identifies the comparison item</param>
        /// <param name="left">Value of the Left property for comparison. Will be not updated if null.</param>
        /// <param name="right">Value of the Right property for comparison. Will be not updated if null.</param>
        void Update(string code, byte[] left, byte[] right);
        /// <summary>
        /// Returns comparison results for specified comparison
        /// </summary>
        /// <param name="id">Id of comparison object from the store for results calculation</param>
        IEnumerable<SequenceDifference> GetResults(long id);
    }
}
