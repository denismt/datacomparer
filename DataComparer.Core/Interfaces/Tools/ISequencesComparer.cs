﻿using DataComparer.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.Core.Interfaces.Tools
{
    public interface ISequencesComparer<T>
    {
        /// <summary>
        /// Comapres two aequences and returns set of information about differences 
        /// </summary>
        /// <param name="left">First sequence for comparison</param>
        /// <param name="right">Second sequence of comparison</param>
        /// <returns>Set of tuples. Each contain start index and length of the different bytes sequence</returns>
        IEnumerable<SequenceDifference> Compare(T left,T right);
    }
}
