﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.Core.Interfaces.Infrastructure
{
    /// <summary>
    /// Represents signature for the dependenies configuration which can be used in the project and cold be understood by infrastructure
    /// </summary>
    public interface IDependenciesConfiguration
    {
        /// <summary>
        /// Collection of the dependencies
        /// </summary>
        IDependencyItem[] DependencyItems { get; }
    }
}
