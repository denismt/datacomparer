﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.Core.Interfaces.Infrastructure
{
    /// <summary>
    /// Providec signature of the item which represents information about one dependency
    /// </summary>
    public interface IDependencyItem
    {
        /// <summary>
        /// Interface which needs to be satisfied
        /// </summary>
        Type Interface { get; }
        /// <summary>
        /// Class which needs to be returned for the interface
        /// </summary>
        Type Implementation { get; }
    }
}
