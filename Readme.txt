
Preparing:

- The connection string in the WebApi project's web.config file must be specified before start (or when deploying).

Input data format:

- Input value must be a JSON object which has Base64Data property containing base64 encoded binary data.
For example: {Base64Data: 'AAAAAQEAAQABAQAAAQABAAABAAAB'}

Usage:

- Each comparison item has it's own string identifier.
- The left value of the item can be set using an URL like http://<host>/v1/diff/<identifier>/left?value=<value>
For example, if you want to set the left value of the comparison with identifier "test" to bytes array { 1,2,3,4,5 } on the development environment, you need to use the URL http://localhost:29455/v1/diff/test/left?value={Base64Data: 'AQIDBAU='}
- The right value of the item can be set using an URL like http://<host>/v1/diff/<identifier>/right?value=<value>
For example, if you want to set the right value of the comparison with identifier "test" to bytes array { 1,2,3,4,5 } on the development environment, you need to use the URL http://localhost:29455/v1/diff/test/right?value={Base64Data: 'AQIDBAU='}
- Both values must be specified before the comparison.
- The information about the differrence can be retrieved by using an URL like http://<host>/v1/diff/<identifier>
For example, if you want to get the difference between left and right values of the item with identifier "test" on the development environment, you need to use the URL http://localhost:29455/v1/diff/test

Output data format:

-Output data is a JSON object containing 3 fields: Result, Message and Data
For example response that indicates that compared sequences are of the same size, but aren't euqal will look like this: {Result: 0, Data: [{StartIndex: 5, Length: 4},{StartIndex: 9,Length: 2}], Message: "Sequences aren't equal.
- "Result" contains a number that indicates was the request processed successfully or not. The value "0" means "OK", the value "1" means "Error".
- "Message" contains text information about request results. If "Result" field has "0" as a value, the "Message field tells the comparison results (diferent sizes, same sizes and are equal, same size and aren't equal). If "Result" equals "1", then the "Message" gives more information about the error. 
- "Data" may contain some additional data. For example exception details in case of error or exact information about the difference if compared values have same size and aren't equal. 
- In case when the "Data" field contains the information about the difference it is represented by a JSON array, Each array element contains 2 fields: StartIndex which shows the index where a different sequence of data starts, Lenght which gives the length of the different sequence.
For example, the difference between two values {1,2,3,4,5,6,7,8,9,10} and {1,2,5,4,3,6,7,8,9,11} will be represented by a following array: [{StartIndex: 2,Length: 3},{StartIndex: 9,Length: 1}]

Rules and logic

- If specified sequences are of the same size and are equal, the "Message" field will contain the text "Sequences are equal.".
- If specified sequences aren't of the same size , the "Message" field will contain the text "Sequence sizes aren't equal.".
- If specified sequences are of the same size and aren't equal, the "Message" field will contain the text "Sequences aren't equal." and the "Data" field will contain the information about the exact difference.