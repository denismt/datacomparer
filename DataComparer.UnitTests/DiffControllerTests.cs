﻿using DataComparer.Core.BusinessObjects;
using DataComparer.Core.Infrastructure;
using DataComparer.Core.Interfaces.Services;
using DataComparer.WebApi.Controllers;
using DataComparer.WebApi.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.UnitTests
{
    /// <summary>
    /// Povides test for the DiffController class
    /// </summary>
    [TestClass]
    public class DiffControllerTests
    {
        /// <summary>
        /// Enshures that a request for setting the left value with empty data will be coorectly refused
        /// </summary>
        [TestMethod]
        public void DiffControllerLeftRefusesEmptyRequest()
        {
            //Arrange
            IComparisonService comparisonService = Mock.Of<IComparisonService>();
            DiffController diffController = new DiffController(comparisonService);
            //Act
            OperationResult result=diffController.Left("someId",null);
            //Assert
            Assert.AreEqual(ResultCodes.Error,result.Result);
        }

        /// <summary>
        /// Enshures that a request for setting the left value with wrong data will be coorectly refused
        /// </summary>
        [TestMethod]
        public void DiffControllerLeftRefusesWrongRequest()
        {
            //Arrange
            IComparisonService comparisonService = Mock.Of<IComparisonService>();
            DiffController diffController = new DiffController(comparisonService);
            string jsonParameter = "abcdefghhgfedcba";
            //Act
            OperationResult result = diffController.Left("someId",jsonParameter);
            //Assert
            Assert.AreEqual(ResultCodes.Error, result.Result);
        }

        /// <summary>
        /// Enshures that a request for setting the left value with correct data will be accepted
        /// </summary>
        [TestMethod]
        public void DiffControllerLeftAcceptsCorrectRequest()
        {
            //Arrange
            IComparisonService comparisonService = Mock.Of<IComparisonService>();
            DiffController diffController = new DiffController(comparisonService);
            string jsonParameter = JsonConvert.SerializeObject(new { Base64Data = Convert.ToBase64String(new byte[] { 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1 }) });
            //Act
            OperationResult result = diffController.Left("someId",jsonParameter);
            //Assert
            Assert.AreEqual(ResultCodes.OK,result.Result);
        }

        /// <summary>
        /// Enshures that a request for setting the right value with empty data will be coorectly refused
        /// </summary>
        [TestMethod]
        public void DiffControllerRightRefusesEmptyRequest()
        {
            //Arrange
            IComparisonService comparisonService = Mock.Of<IComparisonService>();
            DiffController diffController = new DiffController(comparisonService);
            //Act
            OperationResult result = diffController.Right("someId", null);
            //Assert
            Assert.AreEqual(ResultCodes.Error, result.Result);
        }

        /// <summary>
        /// Enshures that a request for setting the right value with wrong data will be coorectly refused
        /// </summary>
        [TestMethod]
        public void DiffControllerRightRefusesWrongRequest()
        {
            //Arrange
            IComparisonService comparisonService = Mock.Of<IComparisonService>();
            DiffController diffController = new DiffController(comparisonService);
            string jsonParameter = "abcdefghhgfedcba";
            //Act
            OperationResult result = diffController.Right("someId", jsonParameter);
            //Assert
            Assert.AreEqual(ResultCodes.Error, result.Result);
        }

        /// <summary>
        /// Enshures that a request for setting the right value with correct data will be accepted
        /// </summary>
        [TestMethod]
        public void DiffControllerRightAcceptsCorrectRequest()
        {
            //Arrange
            IComparisonService comparisonService = Mock.Of<IComparisonService>();
            DiffController diffController = new DiffController(comparisonService);
            string jsonParameter = JsonConvert.SerializeObject(new { Base64Data = Convert.ToBase64String(new byte[] { 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0 }) });
            //Act
            OperationResult result = diffController.Right("someId", jsonParameter);
            //Assert
            Assert.AreEqual(ResultCodes.OK, result.Result);
        }

        /// <summary>
        /// Ensures that in case when some value isn't set Diff action refuses request correctly
        /// </summary>
        [TestMethod]
        public void DiffControllerDiffRefusesIfValueNotSet()
        {
            //Arrange
            Mock<IComparisonService> comparisonServiceMock = new Mock<IComparisonService>();
            comparisonServiceMock.Setup(m => m.GetByCode(It.Is<string>(v=>v=="leftemptyid"))).Returns(new Comparison { LeftValue = null, RightValue = new byte[] { 0, 1, 1 } });
            comparisonServiceMock.Setup(m => m.GetByCode(It.Is<string>(v => v == "rightemptyid"))).Returns(new Comparison { LeftValue = new byte[] { 0, 1, 1 }, RightValue = null });
            comparisonServiceMock.Setup(m => m.GetByCode(It.Is<string>(v => v == "bothemptyid"))).Returns(new Comparison { LeftValue = null, RightValue = null });
            DiffController diffController = new DiffController(comparisonServiceMock.Object);
            //Act
            OperationResult leftemptyresult = diffController.Diff("leftemptyid");
            OperationResult rightemptyresult = diffController.Diff("rightemptyid");
            OperationResult bothemptyresult = diffController.Diff("bothemptyid");
            //Assert
            Assert.AreEqual(ResultCodes.Error, leftemptyresult.Result);
            Assert.AreEqual(ResultCodes.Error, rightemptyresult.Result);
            Assert.AreEqual(ResultCodes.Error, rightemptyresult.Result);
        }

        /// <summary>
        /// Ensures that in case when sequences have different sizes Diff action return just information about that.
        /// </summary>
        [TestMethod]
        public void DiffControllerDiffReturnsSizesAreDifferent()
        {
            //Arrange
            Mock<IComparisonService> comparisonServiceMock = new Mock<IComparisonService>();
            comparisonServiceMock.Setup(m=>m.GetByCode(It.IsAny<string>())).Returns(new Comparison { LeftValue= new byte[] { 0, 1, 1}, RightValue=new byte[] { 0, 0 } });
            DiffController diffController = new DiffController(comparisonServiceMock.Object);
            //Act
            OperationResult result = diffController.Diff("someid");
            //Assert
            Assert.AreEqual(ResultCodes.OK, result.Result);
            Assert.AreEqual("Sequence sizes aren't equal.",result.Message);
        }

        /// <summary>
        /// Ensures that in case when sequences are equal Diff ation returns that
        /// </summary>
        [TestMethod]
        public void DiffControllerDiffReturnsSequencesAreEqual()
        {
            //Arrange
            Mock<IComparisonService> comparisonServiceMock = new Mock<IComparisonService>();
            comparisonServiceMock.Setup(m => m.GetByCode(It.IsAny<string>())).Returns(new Comparison { LeftValue = new byte[] { 0, 1 }, RightValue = new byte[] { 0, 1 } });
            comparisonServiceMock.Setup(m => m.GetResults(It.IsAny<long>())).Returns(new List<SequenceDifference>());
            DiffController diffController = new DiffController(comparisonServiceMock.Object);
            //Act
            OperationResult result = diffController.Diff("someid");
            //Assert
            Assert.AreEqual(ResultCodes.OK, result.Result);
            Assert.AreEqual("Sequences are equal.", result.Message);
        }

        /// <summary>
        /// Ensures that in case when sequences are of same size and not equal Diff action returns information about differences
        /// </summary>
        [TestMethod]
        public void DiffControllerDiffReturnsDifferences()
        {
            //Arrange
            Mock<IComparisonService> comparisonServiceMock = new Mock<IComparisonService>();
            comparisonServiceMock.Setup(m => m.GetByCode(It.IsAny<string>())).Returns(new Comparison { LeftValue = new byte[] { 0, 1 }, RightValue = new byte[] { 0, 1 } });
            comparisonServiceMock.Setup(m => m.GetResults(It.IsAny<long>())).Returns(new List<SequenceDifference> { new SequenceDifference { StartIndex=3,Length=2 }, new SequenceDifference { StartIndex = 15, Length = 123 } });
            DiffController diffController = new DiffController(comparisonServiceMock.Object);
            //Act
            OperationResult result = diffController.Diff("someid");
            //Assert
            Assert.AreEqual(ResultCodes.OK, result.Result);
            IEnumerable<SequenceDifference> data = (IEnumerable<SequenceDifference>)result.Data;
            Assert.IsTrue(data.Contains(new SequenceDifference { StartIndex = 3, Length = 2 }));
            Assert.IsTrue(data.Contains(new SequenceDifference { StartIndex = 15, Length = 123 }));
            Assert.AreEqual(2, data.Count());
        }
    }
}
