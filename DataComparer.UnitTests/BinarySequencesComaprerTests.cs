﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataComparer.Logic.Implementations.Tools;
using DataComparer.Core.Infrastructure;
using Newtonsoft.Json;

namespace DataComparer.UnitTests
{
    /// <summary>
    /// Povides test for the BinarySequencesComparer class
    /// </summary>
    [TestClass]
    public class BinarySequencesComaprerTests
    {
        /// <summary>
        /// Enshures that BinarySequencesComarer compares sequences with different sizes correctly. Firt sequence is longer.
        /// </summary>
        [TestMethod]
        public void BinarySequencesComparerFirstLonger()
        {
            //Arrange
            BinarySequencesComparer comparer = new BinarySequencesComparer();
            byte[] left = new byte[] { 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1 };
            byte[] right = new byte[] { 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1 };
            //Act
            IEnumerable<SequenceDifference> results = comparer.Compare(left, right);
            //Assert
            int differencesCount = results.Count();
            Assert.AreEqual(1,differencesCount);
            SequenceDifference first = results.First();
            Assert.AreEqual(21, first.StartIndex);
            Assert.AreEqual(2, first.Length);
        }

        /// <summary>
        /// Enshures that BinarySequencesComarer compares sequences with different sizes correctly. Second sequence is longer.
        /// </summary>
        [TestMethod]
        public void BinarySequencesComparerSecondLonger()
        {
            //Arrange
            BinarySequencesComparer comparer = new BinarySequencesComparer();
            byte[] left = new byte[] { 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1 };
            byte[] right = new byte[] { 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0 };
            //Act
            IEnumerable<SequenceDifference> results = comparer.Compare(left, right);
            //Assert
            int differencesCount = results.Count();
            Assert.AreEqual(1, differencesCount);
            SequenceDifference first = results.First();
            Assert.AreEqual(21, first.StartIndex);
            Assert.AreEqual(3, first.Length);
        }

        /// <summary>
        /// Enshures that BinarySequencesComarer compares equal sequences correctly.
        /// </summary>
        [TestMethod]
        public void BinarySequencesComparerEqual()
        {
            //Arrange
            BinarySequencesComparer comparer = new BinarySequencesComparer();
            byte[] left = new byte[] { 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1 };
            byte[] right = new byte[] { 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1 };
            //Act
            IEnumerable<SequenceDifference> results = comparer.Compare(left, right);
            //Assert
            int differencesCount = results.Count();
            Assert.AreEqual(0, differencesCount);
        }

        /// <summary>
        /// Enshures that BinarySequencesComarer compares not equal sequences with same size correctly.
        /// </summary>
        [TestMethod]
        public void BinarySequencesComparerSameSizeNotEqual()
        {
            //Arrange
            BinarySequencesComparer comparer = new BinarySequencesComparer();
            byte[] left = new byte[] { 0,0,0,1,1,0,1,0,1,1,0,0,1,0,1,0,0,1,0,0,1 };
            byte[] right = new byte[] { 0,1,1,1,1,0,1,0,0,0,1,1,0,0,1,0,1,1,0,0,1 };
            //Act
            IEnumerable<SequenceDifference> results = comparer.Compare(left, right);
            //Assert
            Assert.IsTrue(results.Contains(new SequenceDifference { StartIndex=1, Length=2 }));
            Assert.IsTrue(results.Contains(new SequenceDifference { StartIndex = 8, Length = 5 }));
            Assert.IsTrue(results.Contains(new SequenceDifference { StartIndex = 16, Length = 1 }));
            Assert.AreEqual(3,results.Count());
            
        }
    }
}
