﻿using DataComparer.Core.Implementations.Infrastructure;
using DataComparer.Core.Interfaces.Infrastructure;
using DataComparer.Core.Interfaces.Services;
using DataComparer.Core.Interfaces.Tools;
using DataComparer.Logic.Implementations.Services;
using DataComparer.Logic.Implementations.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.Logic
{
    /// <summary>
    /// Configures dependencies implemented in the DataComparer.Logic project
    /// </summary>
    public class DependenciesConfiguration: IDependenciesConfiguration
    {
        public IDependencyItem[] DependencyItems { get; }
        public DependenciesConfiguration()
        {
            DependencyItems = new IDependencyItem[]
            {
               new DependencyItem<IComparisonService,ComparisonService>(),
               new DependencyItem<ISequencesComparer<byte[]>,BinarySequencesComparer>()
            };
        }
    }
}
