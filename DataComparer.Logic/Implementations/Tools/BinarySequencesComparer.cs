﻿using DataComparer.Core.Infrastructure;
using DataComparer.Core.Interfaces.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.Logic.Implementations.Tools
{
    public class BinarySequencesComparer: ISequencesComparer<byte[]>
    {
        /// <summary>
        /// Comapres two aequences of bytes and returns set of information about differences 
        /// </summary>
        /// <param name="left">First sequence for comparison</param>
        /// <param name="right">Second sequence of comparison</param>
        /// <returns>Set of tuples. Each contain start index and length of the different bytes sequence</returns>
        public IEnumerable<SequenceDifference> Compare(byte[] left, byte[] right)
        {
            List<SequenceDifference> results = new List<SequenceDifference>();
            long startIndex=-1;
            long length=0;
            int i = 0;
            while(i<left.Length && i<right.Length)
            {
                if (left[i] != right[i])
                {
                    if (length == 0)
                        startIndex = i;
                    length++;
                }
                else
                {
                    if (length > 0)
                    {
                        results.Add(new SequenceDifference { StartIndex = startIndex, Length = length });
                        length = 0;
                    }
                }
                i++;
            }
            if (left.Length > i)
                results.Add(new SequenceDifference { StartIndex = i, Length = left.Length - i });
            if (right.Length > i)
                results.Add(new SequenceDifference { StartIndex = i, Length = right.Length - i });
            return results;
        }
    }
}
