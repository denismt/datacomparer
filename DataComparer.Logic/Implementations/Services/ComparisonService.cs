﻿using DataComparer.Core.BusinessObjects;
using DataComparer.Core.Infrastructure;
using DataComparer.Core.Interfaces.Services;
using DataComparer.Core.Interfaces.Tools;
using DataComparer.Logic.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.Logic.Implementations.Services
{
    /// <summary>
    /// Implements logic for Comparizon business object
    /// </summary>
    class ComparisonService : IComparisonService
    {
        private IComparisonRepository _comparisonRepository;
        private IComparisonResultRepository _comparisonResultRepository;
        private ISequencesComparer<byte[]> _binaryComparer;
        public ComparisonService(IComparisonRepository comparisonRepository, IComparisonResultRepository comparisonResultRepository, ISequencesComparer<byte[]> binaryComaprer)
        {
            _comparisonRepository = comparisonRepository;
            _binaryComparer = binaryComaprer;
            _comparisonResultRepository = comparisonResultRepository;
        }
        /// <summary>
        /// Gets specific comarison by its id
        /// </summary>
        /// <param name="id">Id of the required Comparison boject</param>
        /// <returns></returns>
        public Comparison GetById(long id)
        {
            return _comparisonRepository.GetById(id);
        }

        /// <summary>
        /// Gets specific comarison by its string code (may be called id in UI and endpoints)
        /// </summary>
        /// <param name="code">Code of the required Comparison boject</param>
        /// <returns></returns>
        public Comparison GetByCode(string code)
        {
            return _comparisonRepository.GetByCode(code);
        }

        /// <summary>
        /// Updates specific comparison objects with the specified left and right values or adds new object of need
        /// </summary>
        /// <param name="code">String code (also may be called id in UI and enpoints) which identifies the comparison item</param>
        /// <param name="left">Value of the Left property for comparison. Will be not updated if null.</param>
        /// <param name="right">Value of the Right property for comparison. Will be not updated if null.</param>
        public void Update(string code, byte[] left, byte[] right)
        {
            bool isNew = false;
            Comparison comparison = _comparisonRepository.GetByCode(code);
            if (comparison == null)
            {
                comparison = new Comparison();
                isNew = true;
            }
            if (left != null)
                comparison.LeftValue = left;
            if (right != null)
                comparison.RightValue = right;
            comparison.IsChanged = true;

            if (!isNew)
                _comparisonRepository.Update(comparison);
            else
            {
                comparison.Code = code;
                _comparisonRepository.Add(comparison);
            }
            return;
        }

        /// <summary>
        /// Returns comparison results for specified comparison
        /// </summary>
        /// <param name="comparison">Comparison object from the store for results calculation</param>
        /// <param name="id">Id of comparison object from the store for results calculation</param>
        public IEnumerable<SequenceDifference> GetResults(long id)
        {
            Comparison comparison = _comparisonRepository.GetById(id);
            if (comparison == null)
                return null;
            if (comparison.LeftValue == null || comparison.RightValue == null)
                return null;
            IEnumerable<SequenceDifference> results;
            if (comparison.IsChanged)
            {
                _comparisonResultRepository.Delete(comparison.Id);
                results = _binaryComparer.Compare(comparison.LeftValue, comparison.RightValue);
                foreach (SequenceDifference result in results)
                {
                    _comparisonResultRepository.Add(new ComparisonResult { Start = result.StartIndex, Length = result.Length, ComparisonId = comparison.Id });
                }
                comparison.IsChanged = false;
                _comparisonRepository.Update(comparison);
                return results;
            }
            return _comparisonResultRepository.GetByComparisonId(comparison.Id).Select(cr=> new SequenceDifference { StartIndex=cr.Start, Length=cr.Length });
        }
    }
}
