﻿using DataComparer.Core.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.Logic.Interfaces.Repositories
{
    /// <summary>
    /// Represents signature for the classes which implements data storing functionality for Comparizon business object
    /// </summary>
    public interface IComparisonRepository
    {
        /// <summary>
        /// Returns specific comparison object by its id
        /// </summary>
        /// <param name="code">Id of the comparison in the data store</param>
        /// <returns>Comparison object if found. Otherwise null.</returns>
        Comparison GetById(long id);

        /// <summary>
        /// Returns specific comparison object by its code
        /// </summary>
        /// <param name="code">Unique sting code of comparison (may be called id in UI and enpoints)</param>
        /// <returns>Comparison object if found. Otherwise null.</returns>
        Comparison GetByCode(string code);

        /// <summary>
        /// Updates specific comparison object
        /// </summary>
        /// <param name="comparison">Comparison which needs to be updated</param>
        void Update(Comparison comparison);

        /// <summary>
        /// Creates new comparison object in the data store
        /// </summary>
        /// <param name="comparison">Comparison which needs to be added</param>
        void Add(Comparison comparison);

        /// <summary>
        /// Delete the comparison object from the data store
        /// </summary>
        /// <param name="comparison">Comparison which needs to be deleted</param>
        void Delete(Comparison comparison);
    }
}
