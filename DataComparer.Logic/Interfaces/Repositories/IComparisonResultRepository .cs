﻿using DataComparer.Core.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.Logic.Interfaces.Repositories
{
    /// <summary>
    /// Represents signature for the classes which implements data storing functionality for Comparizon business object
    /// </summary>
    public interface IComparisonResultRepository
    {


        /// <summary>
        /// Creates new comparison result object in the data store
        /// </summary>
        /// <param name="comparisonResult">Comparison result which needs to be added</param>
        void Add(ComparisonResult comparisonResult);

        /// <summary>
        /// Delete the comparison result object from the data store
        /// </summary>
        /// <param name="comparisonResult">Comparison result which needs to be deleted</param>
        void Delete(ComparisonResult comparisonResult);

        /// <summary>
        /// Delete the comparison result objects from the data store by comparison id
        /// </summary>
        /// <param name="comparisonId">Id of the comparison for which we need to delete the results</param>
        void Delete(long comparisonId);

        /// <summary>
        /// Returns set of the results for specific comparison by its id.
        /// </summary>
        /// <param name="comparisonId">Id of the comparison</param>
        IEnumerable<ComparisonResult> GetByComparisonId(long comparisonId);
    }
}
