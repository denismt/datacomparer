﻿using DataComparer.Core.Interfaces.Infrastructure;
using DataComparer.WebFramework.Implementations.Infrastructure;
using Ninject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Http;

namespace DataComparer.WebFramework
{
    /// <summary>
    /// Provides methods required for configuring of the web projects which are included into DataComparer infrastructure
    /// </summary>
    public class DataComparerWebEngine
    {
        /// <summary>
        /// Returns current web project bin directory path
        /// </summary>
        private static string GetBinDirectory()
        {
            if (HostingEnvironment.IsHosted)
            {
                return HttpRuntime.BinDirectory;
            }
            return AppDomain.CurrentDomain.BaseDirectory;
        }

        /// <summary>
        /// Returns all classes which implement IDependencyConfiguration and are included into the projects and libraries involved
        /// </summary>
        private static IEnumerable<Type> GetDependenciesConfigurations()
        {
            IEnumerable<Assembly> allLoadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            List<Assembly> loadedDataComparerAssemblies = allLoadedAssemblies.Where(a => a.FullName.StartsWith("DataComparer.")).ToList();

            //Check that all DataComparer assemblies from the bin folder are loaded and load those which are not.
            List<string> loadedDataComparerAssemblyNames = new List<string>();
            foreach (Assembly a in loadedDataComparerAssemblies)
            {
                loadedDataComparerAssemblyNames.Add(a.FullName);
            }

            string binPath = GetBinDirectory();
            foreach (string dllPath in Directory.GetFiles(binPath, "*.dll"))
            {
                var assemblyName = AssemblyName.GetAssemblyName(dllPath);
                if (assemblyName.FullName.StartsWith("DataComparer.") && !loadedDataComparerAssemblyNames.Contains(assemblyName.FullName))
                {
                    loadedDataComparerAssemblies.Add(AppDomain.CurrentDomain.Load(assemblyName));
                }
            }

            var typesList = new List<Type>();
            Type dependenciesConfigurationInterfaceType = typeof(IDependenciesConfiguration);
            try
            {
                foreach (Assembly a in loadedDataComparerAssemblies)
                {
                    Type[] types = null;
                    try
                    {
                        types = a.GetTypes();
                    }
                    catch
                    {
                    }
                    if (types != null)
                    {
                        typesList.AddRange(types.Where(t => dependenciesConfigurationInterfaceType.IsAssignableFrom(t) && !t.Equals(dependenciesConfigurationInterfaceType)));
                    }
                }
            }
            catch (ReflectionTypeLoadException)
            {
                //TODO: Handle Reflection Type Load exception
            }
            return typesList;
        }

        /// <summary>
        /// Registers all dependencies configured in all involved projects using IDepeneciesConfiguration inplementations
        /// </summary>
        public static void RegisterDependencies()
        {
            IKernel ninjectKernel = new StandardKernel();
            var dcTypes = GetDependenciesConfigurations();
            var dcInstances = new List<IDependenciesConfiguration>();
            foreach (var dcType in dcTypes)
                dcInstances.Add((IDependenciesConfiguration)Activator.CreateInstance(dcType));
            foreach (var dependenciesConfiguration in dcInstances)
            {
                foreach (IDependencyItem dependencyItem in dependenciesConfiguration.DependencyItems)
                {
                    ninjectKernel.Bind(dependencyItem.Interface).To(dependencyItem.Implementation);
                }
            }

            DependencyResolver.SetResolver(new NinjectDependencyResolver(ninjectKernel));
            GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(ninjectKernel);
        }
    }
}
