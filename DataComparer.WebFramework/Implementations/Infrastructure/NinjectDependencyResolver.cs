﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;

namespace DataComparer.WebFramework.Implementations.Infrastructure
{
    /// <summary>
    /// Implements IDependencyResolver functionality for both ASP.NET Web API and ASP.NET MVC uning Ninjec
    /// </summary>
    public class NinjectDependencyResolver : NinjectDependencyScope, System.Web.Http.Dependencies.IDependencyResolver, System.Web.Mvc.IDependencyResolver
    {

        private IKernel _kernel;
        public NinjectDependencyResolver(IKernel kernel): base(kernel)
        {
            _kernel = kernel;
        }

        public IDependencyScope BeginScope()
        {
            return new NinjectDependencyScope(_kernel.BeginBlock());
        }

        void IDisposable.Dispose()
        {
            base.Dispose();
        }
    }
}
