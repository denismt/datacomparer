﻿using Ninject;
using Ninject.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;

namespace DataComparer.WebFramework.Implementations.Infrastructure
{
    /// <summary>
    /// Implements IDependencyScope required for Web API dependecy injection using Ninject
    /// </summary>
    public class NinjectDependencyScope: IDependencyScope
    {
        protected IResolutionRoot  _resolutionRoot;
        public NinjectDependencyScope(IResolutionRoot resolutionRoot)
        {
            _resolutionRoot = resolutionRoot;
        }

        public object GetService(Type serviceType)
        {
            return _resolutionRoot.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _resolutionRoot.GetAll(serviceType);
        }

        public void Dispose()
        {
            var disposable = (IDisposable)_resolutionRoot;
            if (disposable != null) disposable.Dispose();
            _resolutionRoot = null;
        }
    }
}
