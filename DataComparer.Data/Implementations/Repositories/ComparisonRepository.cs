﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataComparer.Logic.Interfaces.Repositories;
using DataComparer.Core.BusinessObjects;

namespace DataComparer.Data.Implementations.Repositories
{
    /// <summary>
    /// Implements data storing functionality for Comparizon business object
    /// </summary>
    class ComparisonRepository : IComparisonRepository
    {
        private EFDbContext _context;
        public ComparisonRepository()
        {
            _context = new EFDbContext();
        }
        /// <summary>
        /// Returns specific comparison object by its id
        /// </summary>
        /// <param name="code">Id of the comparison in the data store</param>
        /// <returns>Comparison object if found. Otherwise null.</returns>
        public Comparison GetById(long id)
        {
            return _context.Comparisons.Where(c => c.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// Returns specific comparison object by its code
        /// </summary>
        /// <param name="code">Unique sting code of comparison (may be called id in UI and enpoints)</param>
        /// <returns>Comparison object if found. Otherwise null.</returns>
        public Comparison GetByCode(string code)
        {
            return _context.Comparisons.Where(c => c.Code == code).FirstOrDefault();
        }

        /// <summary>
        /// Updates specific comparison object
        /// </summary>
        /// <param name="comparison">Comparison which needs to be updated</param>
        public void Update(Comparison comparison)
        {
            if (_context.Entry<Comparison>(comparison).State == System.Data.Entity.EntityState.Modified)
                _context.SaveChanges();
        }

        /// <summary>
        /// Creates new comparison object in the data store
        /// </summary>
        /// <param name="comparison">Comparison which needs to be added</param>
        public void Add(Comparison comparison)
        {
            _context.Comparisons.Add(comparison);
            _context.SaveChanges();
        }

        /// <summary>
        /// Delete the comparison object from the data store
        /// </summary>
        /// <param name="comparison">Comparison which needs to be deleted</param>
        public void Delete(Comparison comparison)
        {
            _context.Comparisons.Remove(comparison);
            _context.SaveChanges();
        }
    }
}
