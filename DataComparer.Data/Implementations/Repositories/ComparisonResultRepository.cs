﻿using DataComparer.Core.BusinessObjects;
using DataComparer.Logic.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.Data.Implementations.Repositories
{
    class ComparisonResultRepository: IComparisonResultRepository
    {
        private EFDbContext _context;
        public ComparisonResultRepository()
        {
            _context = new EFDbContext();
        }

        /// <summary>
        /// Creates new comparison result object in the data store
        /// </summary>
        /// <param name="comparisonResult">Comparison result which needs to be added</param>
        public void Add(ComparisonResult comparisonResult)
        {
            _context.ComparisonResults.Add(comparisonResult);
            _context.SaveChanges();
        }

        /// <summary>
        /// Delete the comparison rsult object from the data store
        /// </summary>
        /// <param name="comparisonResult">Comparison result which needs to be deleted</param>
        public void Delete(ComparisonResult comparisonResult)
        {
            _context.ComparisonResults.Remove(comparisonResult);
            _context.SaveChanges();
        }

        /// <summary>
        /// Delete the comparison result objects from the data store by comparison id
        /// </summary>
        /// <param name="comparisonId">Id of the comparison for which we need to delete the results</param>
        public void Delete(long comparisonId)
        {
            _context.ComparisonResults.RemoveRange(_context.ComparisonResults.Where(cr=>cr.ComparisonId==comparisonId));
            _context.SaveChanges();
        }

        /// <summary>
        /// Returns set of the results for specific comparison by its id.
        /// </summary>
        /// <param name="comparisonId">Id of the comparison</param>
        public IEnumerable<ComparisonResult> GetByComparisonId(long comparisonId)
        {
            return _context.ComparisonResults.Where(cr => cr.ComparisonId == comparisonId);
        }
    }
}
