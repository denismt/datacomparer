﻿using DataComparer.Core.Implementations.Infrastructure;
using DataComparer.Core.Interfaces.Infrastructure;
using DataComparer.Core.Interfaces.Services;
using DataComparer.Data.Implementations.Repositories;
using DataComparer.Logic.Implementations.Services;
using DataComparer.Logic.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.Logic
{
    /// <summary>
    /// Configures dependencies implemented in the DataComparer.Data project
    /// </summary>
    public class DependenciesConfiguration: IDependenciesConfiguration
    {
        public IDependencyItem[] DependencyItems { get; }
        public DependenciesConfiguration()
        {
            DependencyItems = new IDependencyItem[]
            {
               new DependencyItem<IComparisonRepository,ComparisonRepository>(),
               new DependencyItem<IComparisonResultRepository,ComparisonResultRepository>()
            };
        }
    }
}
