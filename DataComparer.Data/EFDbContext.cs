﻿using DataComparer.Core.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.Data
{
    /// <summary>
    /// Provides db functionality using EF framework
    /// </summary>
    internal class EFDbContext : DbContext
    {
        /// <summary>
        /// Comparisons
        /// </summary>
        public DbSet<Comparison> Comparisons { get; set; }
        /// <summary>
        /// ComparisonResults
        /// </summary>
        public DbSet<ComparisonResult> ComparisonResults { get; set; }
        public EFDbContext() : base()
        {
            var type = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
            if (type == null)
                throw new Exception("Do not remove, ensures static reference to System.Data.Entity.SqlServer");
        }
    }
}
