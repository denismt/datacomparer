﻿using DataComparer.Core.Infrastructure;
using DataComparer.WebApi.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.IntegrationTests
{
    /// <summary>
    /// Provides tests of web api functionality on localhost (development environment)
    /// </summary>
    [TestClass]
    public class LocalMachine
    {
        /// <summary>
        /// Ensures that described functionality is working on the localhost (dvelopment environment) when sequence sizes are different
        /// </summary>
        [TestMethod]
        public void IntegrationLocalFunctionalitySizesAreDifferent()
        {
            //Arrange
            WebClient client = new WebClient();
            string leftUrl = string.Format("http://localhost:29455/v1/diff/testdata/left?value={{base64data: '{0}'}}", Convert.ToBase64String(new byte[] { 0,0,0,1,1,0,1,0,1,1,0,0,1,0,1,0,0,1,0,0,1 }));
            string rightUrl = string.Format("http://localhost:29455/v1/diff/testdata/right?value={{base64data: '{0}'}}", Convert.ToBase64String(new byte[] { 0,1,1,1,1,0,1,0,0,0,1,1,0,0,1,0,1,1,0,0,1,1 }));
            string diffUrl = "http://localhost:29455/v1/diff/testdata";
            //Act
            client.DownloadString(leftUrl);
            client.DownloadString(rightUrl);
            OperationResult response = JsonConvert.DeserializeObject<OperationResult>(client.DownloadString(diffUrl));
            //Assert
            Assert.AreEqual(ResultCodes.OK,response.Result);
            Assert.AreEqual("Sequence sizes aren't equal.", response.Message);
        }

        /// <summary>
        /// Ensures that described functionality is working on the localhost (dvelopment environment) with equal sequences
        /// </summary>
        [TestMethod]
        public void IntegrationLocalFunctionalityAreEqual()
        {
            //Arrange
            WebClient client = new WebClient();
            string leftUrl = string.Format("http://localhost:29455/v1/diff/testdata/left?value={{base64data: '{0}'}}", Convert.ToBase64String(new byte[] { 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1 }));
            string rightUrl = string.Format("http://localhost:29455/v1/diff/testdata/right?value={{base64data: '{0}'}}", Convert.ToBase64String(new byte[] { 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1 }));
            string diffUrl = "http://localhost:29455/v1/diff/testdata";
            //Act
            client.DownloadString(leftUrl);
            client.DownloadString(rightUrl);
            OperationResult response = JsonConvert.DeserializeObject<OperationResult>(client.DownloadString(diffUrl));
            //Assert
            Assert.AreEqual(ResultCodes.OK, response.Result);
            Assert.AreEqual("Sequences are equal.", response.Message);
        }

        /// <summary>
        /// Ensures that described functionality is working on the localhost (dvelopment environment) with not equal sequences of the same size
        /// </summary>
        [TestMethod]
        public void IntegrationLocalFunctionalitySameSizeNotEqual()
        {
            //Arrange
            WebClient client = new WebClient();
            string leftUrl = string.Format("http://localhost:29455/v1/diff/testdata/left?value={{base64data: '{0}'}}", Convert.ToBase64String(new byte[] { 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1 }));
            string rightUrl = string.Format("http://localhost:29455/v1/diff/testdata/right?value={{base64data: '{0}'}}", Convert.ToBase64String(new byte[] { 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1 }));
            string diffUrl = "http://localhost:29455/v1/diff/testdata";
            //Act
            client.DownloadString(leftUrl);
            client.DownloadString(rightUrl);
            OperationResult response = JsonConvert.DeserializeObject<OperationResult>(client.DownloadString(diffUrl));
            IEnumerable<SequenceDifference> results = ((JArray)response.Data).Select(r => new SequenceDifference { StartIndex = (long)r["StartIndex"], Length = (long)r["Length"] });
            //Assert
            Assert.IsTrue(results.Contains(new SequenceDifference { StartIndex = 1, Length = 2 }));
            Assert.IsTrue(results.Contains(new SequenceDifference { StartIndex = 8, Length = 5 }));
            Assert.IsTrue(results.Contains(new SequenceDifference { StartIndex = 16, Length = 1 }));
            Assert.AreEqual(3, results.Count());
        }
    }
}
