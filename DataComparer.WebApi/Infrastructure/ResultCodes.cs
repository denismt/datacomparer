﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.WebApi.Infrastructure
{
    /// <summary>
    /// Contain list of possible API call result codes
    /// </summary>
    public static class ResultCodes
    {
        public const short OK = 0;
        public const short Error = 1;
    }
}
