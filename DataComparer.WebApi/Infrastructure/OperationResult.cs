﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.WebApi.Infrastructure
{
    /// <summary>
    /// Object which structurised return values of the API
    /// </summary>
    public class OperationResult
    {
        /// <summary>
        /// HttpResult code
        /// </summary>
        public short Result { get; set; }
        /// <summary>
        /// Result data of the operation if applicable
        /// </summary>
        public object Data { get; set; }
        /// <summary>
        /// Test message which provides more information
        /// </summary>
        public string Message { get; set;  }
    }
}
