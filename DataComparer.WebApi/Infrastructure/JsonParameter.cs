﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataComparer.WebApi.Infrastructure
{
    /// <summary>
    /// Class that represent the json format required for the Comparer controllers
    /// </summary>
    public class JsonParameter
    {
        /// <summary>
        /// Base 64 encoded data
        /// </summary>
        public string Base64Data { get; set; }
    }
}
