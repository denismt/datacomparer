﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using DataComparer.Core.Interfaces;
using Newtonsoft.Json;
using DataComparer.WebApi.Infrastructure;
using DataComparer.Core.Interfaces.Services;
using DataComparer.Core.BusinessObjects;
using DataComparer.Core.Infrastructure;

namespace DataComparer.WebApi.Controllers
{
    public class DiffController : ApiController
    {
        IComparisonService _comparisonService;

        public DiffController(IComparisonService comparisonService)
        {
            _comparisonService = comparisonService;
        }

        /// <summary>
        /// Accepts JSON with base64 encoded binary data and updates left value for the comparison with specified id
        /// </summary>
        /// <param name="id">String id of the comarison</param>
        /// <param name="value">JSON containing base64 encoded binary data</param>
        [HttpGet]
        public OperationResult Left(string id, string value=null)
        {
            if (value==null)
                return new OperationResult
                {
                    Result = ResultCodes.Error,
                    Message ="The value wasn't specified"
                };
            JsonParameter parameter;
            byte[] data;
            try
            {
                parameter = JsonConvert.DeserializeObject<JsonParameter>(value);
                data=Convert.FromBase64String(parameter.Base64Data);
            }
            catch
            {
                return new OperationResult
                {
                    Result = ResultCodes.Error,
                    Message = "Wrong data format. This endpoint accepts JSON containing Base64Data property with base64 encoded data."
                };
            }

            try {
                _comparisonService.Update(id, data, null);
            }
            catch(Exception ex)
            {
                return new OperationResult
                {
                    Result = ResultCodes.Error,
                    Data=ex.Message,
                    Message = "Some exception occured while processing your request. See 'Data' field for exception details."
                };
            }
            return new OperationResult
            {
                Result = ResultCodes.OK,
                Message = "Value was successfuly updated."
            };
        }


        /// <summary>
        /// Accepts JSON with base64 encoded binary data and updates right value for the comparison with specified id
        /// </summary>
        /// <param name="id">String id of the comarison</param>
        /// <param name="value">JSON containing base64 encoded binary data</param>
        [HttpGet]
        public OperationResult Right(string id, string value = null)
        {
            if (value == null)
                return new OperationResult {
                    Result = ResultCodes.Error,
                    Message = "The value wasn't specified"
                };
            JsonParameter parameter;
            byte[] data;
            try
            {
                parameter = JsonConvert.DeserializeObject<JsonParameter>(value);
                data = Convert.FromBase64String(parameter.Base64Data);
            }
            catch
            {
                return new OperationResult
                {
                    Result = ResultCodes.Error,
                    Message = "Wrong data format. This endpoint accepts JSON containing Base64Data property with base64 encoded data."
                };
            }

            try
            {
                _comparisonService.Update(id, null, data);
            }
            catch (Exception ex)
            {
                return new OperationResult
                {
                    Result = ResultCodes.Error,
                    Data = ex.Message,
                    Message = "Some exception occured while processing your request. See 'Data' field for exception details."
                };
            }
            return new OperationResult
            {
                Result = ResultCodes.OK,
                Message = "Value was successfuly updated."
            };
        }

        /// <summary>
        /// Returns JSON with information about the set of differences between left and right values for the comparison with specified id
        /// </summary>
        /// <param name="id">String id of the comarison</param>
        [HttpGet]
        public OperationResult Diff(string id)
        {
            Comparison comparison;
            try
            {
                comparison = _comparisonService.GetByCode(id);
            }
            catch (Exception ex)
            {
                return new OperationResult
                {
                    Result = ResultCodes.Error,
                    Data = ex.Message,
                    Message = "Some exception occured while processing your request. See 'Data' field for exception details."
                };
            }
            if (comparison==null || comparison.LeftValue==null || comparison.RightValue==null)
                return new OperationResult
                {
                    Result = ResultCodes.Error,
                    Message = "One or both of values for comarison weren't set. Please, use /v1/diff/<your id>/left?value=<json with your value> and /v1/diff/<your id>/right?value=<json with your value> to set the values."
                };
            if (comparison.LeftValue.Length != comparison.RightValue.Length)
                return new OperationResult
                {
                    Result = ResultCodes.OK,
                    Message = "Sequence sizes aren't equal."
                };
            IEnumerable<SequenceDifference> differences;
            try
            {
                differences = _comparisonService.GetResults(comparison.Id);
            }
            catch (Exception ex)
            {
                return new OperationResult
                {
                    Result = ResultCodes.Error,
                    Data = ex.Message,
                    Message = "Some exception occured while processing your request. See 'Data' field for exception details."
                };
            }
            if (differences.Count()==0)
                return new OperationResult
                {
                    Result = ResultCodes.OK,
                    Message = "Sequences are equal."
                };
            return new OperationResult
            {
                Result = ResultCodes.OK,
                Message="Sequences aren't equal.",
                Data= differences
            };
        }
    }
}
